#ifndef STACK
#define STACK

#include <iostream>
using namespace std;

template <typename T> class Stack{

    private:
        //Массив с данными
        T* data;
        //Индекс последнего добавленного
        //элемента
        int top;
        //Размер стека
        int size;
        //На сколько элементов увеличить
        //размер стека, если он полон
        static int incr;

    public:
        Stack(int s = 10);
        ~Stack();
        //Добавление элемента
        void push(const T&);
        //Получение элемента
        bool pop(T&);
        //Проверка полноты стека
        bool is_full() const;
        //Проверка пустоты стека
        bool is_empty() const;

        //Перегрузка оператора вывода
        template <typename T1>
        friend ostream& operator<<(ostream&,const Stack<T1> &st);

        //Метод для вывода
        void print();
};

//Метод для вывода
template <typename T>
void Stack<T>::print()
{
    if (is_empty())
    {
        cout << "Empty stack!" << endl;
        return out;
    }

    for (int i=0;i<top;++i)
        cout << data[i] << "\t";
    cout << endl;
}

//Stack<int> st;
//cout << st; <=> operator<<(cout,st)
//Перегрузка оператора вывода
template <typename T>
ostream& operator<<(ostream& out,const Stack<T> &st)
{
    if (st.is_empty())
    {
        out << "Empty stack!" << endl;
        return out;
    }

    for (int i=0;i<st.top;++i)
        out << st.data[i] << "\t";
    out << endl;
    return out;
}

template <typename T>
int Stack<T>::incr = 10;

//Конструктор
//Все методы шаблона класса
//являются шаблонами функций
template <typename T>
Stack<T>::Stack(int s)
{
    //Проверка целостности
    size = (s > 0) ? s : 10;
    data = new T[size];
    top = 0;
}

//Деструктор
template <typename T>
Stack<T>::~Stack()
{
    if (data != nullptr)
        delete[] data;
}

//Добавление элемента
template <typename T>
void Stack<T>::push(const T& item)
{
    //Если индекс вершины стека
    //меньше размера стека
    if (top < size)
    {
        data[top] = item;
        ++top;
    }
    else
    {
        T* tmp = new T[size+incr];
        for (int i=0;i<size;++i)
            tmp[i] = data[i];
        tmp[top] = item;
        ++top;
        size += incr;
        delete[] data;
        data = tmp;
    }
}

//Получение значения элемента
//на вершине стека
template<typename T>
bool Stack<T>::pop(T& item)
{
    if (top == 0)
        return false;
    item = data[--top];
    return true;
}

//Проверка на полноту
template <typename T>
bool Stack<T>::is_full() const
{
    return (top == size);
}

//Проверка на пустоту
template <typename T>
bool Stack<T>::is_empty() const
{
    return (top == 0);
}

#endif // STACK

