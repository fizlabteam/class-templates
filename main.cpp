#include <iostream>
#include "stack.h"
#include <cstring>

using namespace std;
// 7 + (5 * 9)
// 7 5 9 * +
// 7 14 *

int calculate(char *str)
{
    int n = strlen(str);
    Stack<int> st(32);
    //Просматриваем строку посимвольно
    for (int i=0;i<n;++i)
    {
        //Если символ является цифрой,
        //добавляем ее в стек
        if (isdigit(str[i]))
            st.push(str[i] - '0');
        //Если символ является операцией
        else if (str[i] == '+' || str[i] == '*')
        {
            //Вынимаем два последних числа
            //с вершины стека
            int num1, num2;
            if (!st.pop(num1))
            {
                cout<<"error!"<<endl;
                return 0;
            }
            if (!st.pop(num2))
            {
                cout<<"error!"<<endl;
                return 0;
            }
            //Производим с ними соответствующую операцию
            //результат возвращаем в стек
            if (str[i] == '+')
                st.push(num1+num2);
            else
                st.push(num1*num2);
        }
    }
    //Результат вычислений в итоге
    //должен оказаться на вершине стека
    int res;
    if (st.pop(res))
        return res;
    else
    {
        cout <<"Error!" <<endl;
        return 0;
    }
}

int main()
{
    cout << "Hello World!" << endl;

    Stack<int> st;
    //st.print();
    cout << st;
    st.push(100);
    st.push(500);
    //st.print();
    cout << st;

    int item = 0;
    if (st.pop(item))
        cout <<item << endl;
    char str[64] = "7 5 9 + *";
    cout << calculate(str) << endl;


    //system("pause");
    return 0;
}

